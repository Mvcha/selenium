package models;

public class TableData implements Comparable<TableData>{

    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String email;
    private String residency;
    private String occupation;

    public TableData(String firstName, String lastName, String dateOfBirth, String email, String residency, String occupation) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.residency = residency;
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return  firstName + ' ' +lastName +' '+ dateOfBirth +' '+email +' '+residency+' '+ occupation;
    }

    @Override
    public int compareTo(TableData o) {
        return firstName.compareTo(o.firstName);
    }
    public int compareByLastName(TableData o) {
        return lastName.compareTo(o.lastName);
    }
    public int compareByDateOfBirth(TableData o) {
        return dateOfBirth.compareTo(o.dateOfBirth);
    }
    public int compareByEmail(TableData o) {
        return email.compareTo(o.email);
    }
    public int compareByResidency(TableData o) {
        return residency.compareTo(o.residency);
    }
    public int compareByOccupation(TableData o) {
        return occupation.compareTo(o.occupation);
    }
}
