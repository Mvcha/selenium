package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.apache.commons.compress.harmony.pack200.NewAttribute;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentManager extends BasePage {

    public static ExtentReports extentReports;
    public static ExtentTest test;
    public static String extentReportPrefix;
    public static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();
    public ExtentManager(WebDriver driver) {
        super(driver);
    }

    public synchronized static ExtentReports createExtentReports(String testName)
    {
        if(extentReports==null)
        {
            extentReports=new ExtentReports();
            ExtentSparkReporter reporter=new ExtentSparkReporter(System.getProperty("user.dir")+"\\target\\reports\\report_"+extentReportPrefix(testName)+".html");
            reporter.config().setReportName("Automation Testing Website Report");
            extentReports.attachReporter(reporter);
            extentReports.setSystemInfo("Author", "Marek Mucha");
            extentReports.flush();
        }

        return extentReports;
    }
    public static String extentReportPrefix(String testName)
    {
        String date = new SimpleDateFormat("dd-MM-YYYY_HH-mm-ss").format(new Date());
        extentReportPrefix=testName+"_"+date;
        return extentReportPrefix;
    }
    public synchronized static ExtentTest getTest(){
        return extentTest.get();
    }

    public synchronized static void log(String message)
    {
        getTest().info(message);
    }
    public synchronized static void pass(String message){
        getTest().pass(message);
    }
    public synchronized static void fail(String message){
        getTest().fail(message);
    }
    public synchronized static ExtentTest createTest(String testName)
    {
        test = extentReports.createTest(testName, "Description");
        //extentReports.createTest("test");
        extentTest.set(test);
        return getTest();
    }

    public synchronized static void attachScreenShot(String testName) throws IOException {
        String name = extentReportPrefix(testName);
        BasePage.takeScreenShot(name);
        test.fail(MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\target\\reports\\screenshots\\"+name+".png").build());

    }
    public static void flushReport()
    {
        extentReports.flush();
    }


}
