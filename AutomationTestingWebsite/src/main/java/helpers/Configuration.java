package helpers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static java.lang.System.getProperty;

public class Configuration {
    private static Properties properties;
    private static String appUrl;
   /* private static String getProperties(String propertyName){

    }*/
    public static String getProperties(String nameProperty)
   {
       properties = new Properties();
       try{
           FileInputStream fileInputStream= new FileInputStream(getProperty("user.dir")+"/src/main/resources/config.properties");
           properties.load(fileInputStream);
           return properties.getProperty(nameProperty);
       } catch (IOException e) {
           throw new RuntimeException(e);
       }
   }
    public static String getAppUrl()
    {
        return getProperties("appUrl");
    }

    public static int getDefaultWaitTime(){
        return Integer.parseInt(getProperties("defaultWait"));
    }

}
