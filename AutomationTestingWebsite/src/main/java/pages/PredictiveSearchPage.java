package pages;

import dev.failsafe.internal.util.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class PredictiveSearchPage extends BasePage{

    @FindBy(id = "myInput")
    private WebElement countryInput;
    @FindBy(css = "[onclick]")
    private WebElement submitButton;
    @FindBy(id = "info")
    private WebElement textInfo;

    @FindBy(css = "#myInputautocomplete-list > div")
    private List<WebElement> inputCompleteList;

    @FindBy(id ="myInputautocomplete-list" )
    private WebElement resultsDropDown;

    public void enterText(char letter){
        {
            countryInput.clear();
            countryInput.sendKeys(String.valueOf(letter));
        }
    }
    public boolean enterLetterAndCheckCorrectList()
    {
        for(char alphabet='A'; alphabet<'Z'; alphabet++)
        {
            enterText(alphabet);
            if(inputCompleteList.size()>0)
            {
                for (WebElement wordFromList: inputCompleteList) {
                    char firstLetter= wordFromList.getText().charAt(0);
                    if(firstLetter!=alphabet)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean checkingVisibilityChosenElement(){
        String chosenElement;
        String displayedText;
        int size;
        List<String> listPredictiveSearch= new ArrayList<>();
        for(char alphabet='a'; alphabet<'z'; alphabet++) {
            enterText(alphabet);
            waitToBeVisible(By.id("myInputautocomplete-list"));

            if (inputCompleteList.size() > 0) {
                size=inputCompleteList.size();
                for (WebElement wordFromList : inputCompleteList) {
                    listPredictiveSearch.add(wordFromList.getText());
                }

                for(int i =0; i< size; i++)
                {
                    inputCompleteList.get(i).click();
                    chosenElement=listPredictiveSearch.get(i);
                    waitToBeInvisible(By.id("myInputautocomplete-list"));
                    submitButton.click();
                    displayedText = textInfo.getText();
                    displayedText = displayedText.replace("You selected: ", "");
                    try {
                        waitToVisibilityOfElementWithText(By.id("info"), "You selected: "+chosenElement);
                    }
                    catch (TimeoutException timeoutException)
                    {
                        return false;
                    }
                    if (!chosenElement.equals(displayedText)) {
                        return false;
                    }
                    inputCompleteList.clear();
                    enterText(alphabet);
                }
                listPredictiveSearch.clear();
            }
        }
        return true;
    }


    public PredictiveSearchPage(WebDriver driver) {
        super(driver);
    }
}
