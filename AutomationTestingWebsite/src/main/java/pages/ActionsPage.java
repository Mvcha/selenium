package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ActionsPage extends BasePage {

    @FindBy(id="dragtarget")
    private WebElement dragInitialRectangle;

    @FindBy(css=".inner > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(2)")
    private WebElement dragTargetRectangle;

    @FindBy(css ="#click-box")
    private WebElement clickRectangle;
    @FindBy(id ="doubClickStartText")
    private WebElement doubleClickRectangle;

    @FindBy(css="[onmousedown] #doubClickStartText")
    private WebElement shiftAndClickRectangle;

    public ActionsPage(WebDriver driver) {
        super(driver);
    }

    public void dragAndDrop()
    {
        int x = dragTargetRectangle.getLocation().x;
        int y = dragTargetRectangle.getLocation().y;
        Rectangle start = dragInitialRectangle.getRect();
        Rectangle finish = dragTargetRectangle.getRect();
        System.out.println("x= "+x +" y= "+y);
     //   actions.dragAndDrop(dragInitialRectangle, dragTargetRectangle).build().perform();
        /*actions.clickAndHold(dragInitialRectangle)
                .moveToElement(dragTargetRectangle)
                .release(dragInitialRectangle)
                .build()
                .perform();
        actions.dragAndDropBy(dragInitialRectangle, finish.getX() - start.getX(), finish.getY() - start.getY())
                .perform();*/
        Action dragAndDrop = actions.clickAndHold(dragInitialRectangle)
                .moveByOffset(-1, -1)
                .moveToElement(dragTargetRectangle)
                .release()
                .build();

        dragAndDrop.perform();
    }

    public void clickAndHold()
    {
        actions.clickAndHold(clickRectangle)
                .perform();
    }

    public void doubleClick()
    {
        actions.doubleClick(doubleClickRectangle).perform();
    }
    public void shiftAndClick()
    {
        actions.keyDown(Keys.LEFT_SHIFT).click(shiftAndClickRectangle).perform();
        wait.until(ExpectedConditions.alertIsPresent());
    }
}
