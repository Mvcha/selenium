package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class DropDownCheckboxPage extends BasePage {

    @FindBy(css="[for='demo-priority-low']")
    private WebElement firstRadioButton;
    @FindBy(css = "[for='demo-priority-normal']")
    private WebElement secondRadioButton;
    @FindBy(css = "[for='demo-priority-high']")
    private WebElement thirdRadioButton;

    @FindBy(css = "[for='cb_red']")
    private WebElement firstCheckBox;
    @FindBy(css = "[for='cb_green']")
    private WebElement secondCheckBox;
    @FindBy(css = "[for='cb_blue']")
    private WebElement thirdCheckBox;
    @FindBy (linkText = "Home")
    private WebElement homeMenuItem;
    @FindBy(linkText = "Animals")
    private WebElement animalsMenuItem;
    @FindBy(linkText = "Sports")
    private WebElement sportsMenuItem;

    @FindBy(css = "li:nth-of-type(2) > ul > li")
    private List<WebElement> animalsMenuElements;
    @FindBy(css = "li:nth-of-type(3) > ul > li")
    private List<WebElement> sportsMenuElements;
    @FindBy(id = "outputMessage")
    private WebElement outputMessage;
    @FindBy(id = "cars")
    private WebElement carSelect;
    @FindBy(css = "select#cars > option")
    private List<WebElement> carsElements;

    public String chooseCarFromDropdown(String carName)
    {
        Select select = new Select(carSelect);
        select.selectByVisibleText(carName);
        WebElement option = select.getFirstSelectedOption();
        return option.getText();
    }

    public void chooseAnimalFromMenu(String animalName){

       actions.moveToElement(animalsMenuItem);
       actions.build().perform();

        for (WebElement animalsElement1 : animalsMenuElements) {
            if(animalsElement1.getText().equals(animalName))
            {
                animalsElement1.click();
                waitToBeVisible(By.id("outputMessage"));
            }
        }

    }


    public DropDownCheckboxPage(WebDriver driver) {
        super(driver);
    }

    public void clickRadioButton(int number)
    {
        chooseOption(number, firstRadioButton, secondRadioButton, thirdRadioButton);
    }

    public void clickCheckbox(int number)
    {
        chooseOption(number, firstCheckBox, secondCheckBox, thirdCheckBox);
    }

    private void chooseOption(int number, WebElement webElement1, WebElement webElement2, WebElement webElement3) {
        switch (number)
        {
            case 1: click(webElement1);
                break;
            case 2: click(webElement2);
                break;
            case 3: click(webElement3);
                break;
        }
    }
}
