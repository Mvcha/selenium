package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ContactUsPage extends BasePage{


    @FindBy (name = "first_name")
    private WebElement firstNameField;
    @FindBy (name = "last_name")
    private WebElement lastNameField;
    @FindBy (name = "email")
    private WebElement emailField;
    @FindBy (name = "message")
    private WebElement messageField;
    @FindBy(css = "[type='reset']")
    private WebElement resetButton;
    @FindBy(css = "[type='submit']")
    private WebElement submitButton;


    public void fillForm(String firstName, String lastName, String email, String comment){
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        emailField.sendKeys(email);
        messageField.sendKeys(comment);
    }

    public String getFirstName()
    {
        return firstNameField.getAttribute("value");
    }
    public String getLastName()
    {
        return lastNameField.getAttribute("value");
    }
    public String getEmail()
    {
        return emailField.getAttribute("value");
    }
    public String getComment()
    {
        return messageField.getAttribute("value");
    }
    public void clearForm()
    {
        click(resetButton);
    }
    public void submitForm()
    {
        click(submitButton);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("h3"),"Thank you for your mail!"));
    }

    public ContactUsPage(WebDriver driver) {
        super(driver);
    }
}
