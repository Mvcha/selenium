package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HiddenElementsPage extends BasePage{

    @FindBy(xpath = "//div[@id='main']/div[@class='inner']//button[.='Toggle']")
    private WebElement toggleButton;
    @FindBy(id = "myDIV")
    private WebElement hiddenParagraphs;


    public String showHiddenParagraph()
    {
        click(toggleButton);
        waitToBeVisible(hiddenParagraphs);
        return hiddenParagraphs.getText();
    }
    public HiddenElementsPage(WebDriver driver) {
        super(driver);
    }
}
