package pages;

import helpers.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class LoaderTwoPage extends BasePage{


    @FindBy(id = "appears")
    private WebElement textToAppear;



    public String clickMeAfterLoader()
    {
        WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait2.until(ExpectedConditions.visibilityOf(textToAppear));

        return textToAppear.getText();
    }
    public LoaderTwoPage(WebDriver driver) {
        super(driver);
    }
}
