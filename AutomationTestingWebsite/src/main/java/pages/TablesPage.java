package pages;

import models.TableData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TablesPage extends BasePage{

    @FindBy(xpath = "//th[1]")
    private WebElement firstNameHeadline;
    @FindBy(xpath = "//th[2]")
    private WebElement lastNameHeadline;
    @FindBy(xpath = "//th[3]")
    private WebElement dateOfBirthHeadline;
    @FindBy(xpath = "//th[4]")
    private WebElement emailHeadline;
    @FindBy(xpath = "//th[5]")
    private WebElement residencyHeadline;
    @FindBy(xpath = "//th[6]")
    private WebElement occupationHeadline;

    @FindBy(css = "tbody tr")
    private List<WebElement> rows;
    @FindBy(css = "thead tr")
    private List<WebElement> columns;
    @FindBy(css = "tbody td")
    private List<WebElement> columnsData;

    private  List<TableData> records = new ArrayList<>();


    public TablesPage(WebDriver driver) {
        super(driver);
    }

    public String actualDataFromTableAsString()
    {
        List<String> allDataFromTable = new ArrayList<>();
        for (WebElement row: rows)
        {
            allDataFromTable.add(row.getText());
        }

        return allDataFromTable.toString();
    }

    public void addToModel() {

        String[] dataFromTable = new String[6];

        for (int i = 1; i <= rows.size(); i++) {
            for (int j = 1; j < 7; j++) {
                dataFromTable[j - 1] = driver.findElement(By.cssSelector("tr:nth-of-type("+i+") > td:nth-of-type("+j+")")).getText();
            }
            records.add(new TableData(dataFromTable[0], dataFromTable[1], dataFromTable[2], dataFromTable[3], dataFromTable[4], dataFromTable[5]));
        }

    }
    public List<TableData> sortBy(String whichColumn, boolean ascending){
        List<TableData> sortedList = new ArrayList<>();
        switch (whichColumn)
        {
            case "firstName":
                sortedList=sortByFirstName(ascending);
                break;
            case "lastName":
                sortedList=sortByLastName(ascending);
                break;
            case "dateOfBirth":
                sortedList=sortByDateOfBirth(ascending);
                break;
            case "email":
                sortedList=sortByEmail(ascending);
                break;
            case "residency":
                sortedList=sortByResidency(ascending);
                break;
            case "occupation":
                sortedList=sortByOccupation(ascending);
                break;
        }
        return sortedList;
    }

    public List<TableData> sortByFirstName(boolean ascending){
        if(ascending==true)
        {
            Collections.sort(records);
            click(firstNameHeadline);

        }
        else
        {
            Collections.sort(records, Collections.reverseOrder());
            click(firstNameHeadline);
            click(firstNameHeadline);
        }

        return records;
    }

    public List<TableData> sortByLastName(boolean ascending)
    {
        if(ascending==true)
        {

            Collections.sort(records, TableData::compareByLastName);
            click(lastNameHeadline);
        }
        else
        {
            Collections.sort(records, Collections.reverseOrder(TableData::compareByLastName));
            click(lastNameHeadline);
            click(lastNameHeadline);
        }
        return records;
    }
    public List<TableData> sortByDateOfBirth(boolean ascending)
    {
        System.out.println("Jestem tutaj:");
        if(ascending==true)
        {

            Collections.sort(records, TableData::compareByDateOfBirth);
            click(dateOfBirthHeadline);
        }
        else
        {
            Collections.sort(records, Collections.reverseOrder(TableData::compareByDateOfBirth));
            click(dateOfBirthHeadline);
            click(dateOfBirthHeadline);
        }
        return records;
    }
    public List<TableData> sortByEmail(boolean ascending)
    {
        if(ascending==true)
        {

            Collections.sort(records, TableData::compareByEmail);
            click(emailHeadline);
        }
        else
        {
            Collections.sort(records, Collections.reverseOrder(TableData::compareByEmail));
            click(emailHeadline);
            click(emailHeadline);
        }
        return records;
    }
    public List<TableData> sortByResidency(boolean ascending)
    {
        if(ascending==true)
        {

            Collections.sort(records, TableData::compareByResidency);
            click(residencyHeadline);
        }
        else
        {
            Collections.sort(records, Collections.reverseOrder(TableData::compareByResidency));
            click(residencyHeadline);
            click(residencyHeadline);
        }
        return records;
    }
    public List<TableData> sortByOccupation(boolean ascending)
    {
        if(ascending==true)
        {

            Collections.sort(records, TableData::compareByOccupation);
            click(occupationHeadline);
        }
        else
        {
            Collections.sort(records, Collections.reverseOrder(TableData::compareByOccupation));
            click(occupationHeadline);
            click(occupationHeadline);
        }
        return records;
    }

}
