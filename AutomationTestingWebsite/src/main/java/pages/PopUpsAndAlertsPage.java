package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;

public class PopUpsAndAlertsPage extends BasePage{

    @FindBy(css = "[onclick='popup\\(\\)']")
    private WebElement triggerPopUpButton;

    @FindBy(xpath = "//div[@id='main']/div[@class='inner']//button[.='Trigger Alert']")
    private WebElement triggerAlertButton;

    public int firstTriggerPopUp()
    {

        String originalWindow = driver.getWindowHandle();
        click(triggerPopUpButton);
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        ArrayList<String> browserWindows = new ArrayList<>(driver.getWindowHandles() );
        driver.switchTo().window(browserWindows.get(1));
        driver.close();
        driver.switchTo().window(originalWindow);

        return driver.getWindowHandles().size();
    }

    public String secondTriggerAlert()
    {
        click(triggerAlertButton);
        String alertText=driver.switchTo().alert().getText();
        driver.switchTo().alert().accept();
        return alertText;
    }

    public PopUpsAndAlertsPage(WebDriver driver) {
        super(driver);
    }
}
