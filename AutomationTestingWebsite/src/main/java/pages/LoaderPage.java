package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoaderPage extends BasePage{

    @FindBy(id = "loader")
    WebElement loader;
    @FindBy(id = "h2_wording")
    WebElement header;
    @FindBy(id = "loaderBtn")
    WebElement loaderButton;
    @FindBy(id = "p_wording")
    WebElement text;



    public String clickMeAfterLoader()
    {
        waitToBeInvisible(By.id("loader"));
        loaderButton.click();

        return text.getText();
    }
    public LoaderPage(WebDriver driver) {
        super(driver);
    }
}
