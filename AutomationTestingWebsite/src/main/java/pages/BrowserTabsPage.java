package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class BrowserTabsPage extends BasePage{


    @FindBy(css = "input")
    private WebElement openTabButton;

    public BrowserTabsPage(WebDriver driver) {
        super(driver);
    }

    public void openNewTab(){
        openTabButton.click();
        ArrayList <String> browserTabs = new ArrayList<>(driver.getWindowHandles() );

        driver.switchTo().window(browserTabs.get(1));
        waitToBeVisible(By.cssSelector("button#L2AGLb > div[role='none']"));
  }


}
