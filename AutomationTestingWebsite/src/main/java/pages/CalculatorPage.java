package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CalculatorPage extends BasePage{

    @FindBy(css = "input[value='1']")
    private WebElement oneButton;
    @FindBy(css = "input[value='2']")
    private WebElement twoButton;
    @FindBy(css = "input[value='3']")
    private WebElement threeButton;
    @FindBy(css = "input[value='4']")
    private WebElement fourButton;
    @FindBy(css = "input[value='5']")
    private WebElement fiveButton;
    @FindBy(css = "input[value='6']")
    private WebElement sixButton;
    @FindBy(css = "input[value='7']")
    private WebElement sevenButton;
    @FindBy(css = "input[value='8']")
    private WebElement eightButton;
    @FindBy(css = "input[value='9']")
    private WebElement nineButton;
    @FindBy(css = "input[value='0']")
    private WebElement zeroButton;
    @FindBy(css = "input[value='c']")
    private WebElement clearButton;
    @FindBy(css = "input[value='/']")
    private WebElement divideButton;
    @FindBy(css = "input[value='-']")
    private WebElement minusButton;
    @FindBy(css = "input[value='+']")
    private WebElement plusButton;
    @FindBy(css = "input[value='*']")
    private WebElement multiplicationButton;
    @FindBy(css = "input[value='=']")
    private WebElement equalButton;
    @FindBy(css = "input[value='.']")
    private WebElement dotButton;
    @FindBy(id = "result")
    private WebElement resultField;

    public CalculatorPage(WebDriver driver) {
        super(driver);
    }

    public void chooseNumberFromKeyboard(int number)
    {
        switch (number)
        {
            case 1: oneButton.click();
                break;
            case 2: twoButton.click();
                break;
            case 3: threeButton.click();
                break;
            case 4: fourButton.click();
                break;
            case 5: fiveButton.click();
                break;
            case 6: sixButton.click();
                break;
            case 7: sevenButton.click();
                break;
            case 8: eightButton.click();
                break;
            case 9: nineButton.click();
                break;
            case 0: zeroButton.click();
                break;

        }
    }

    public void enterSymbol(char symbol)
    {
        switch (symbol)
        {
            case '+': plusButton.click();
                break;
            case '-': minusButton.click();
                break;
            case '*': multiplicationButton.click();
                break;
            case '/': divideButton.click();
                break;
            case '=': equalButton.click();
                break;
            case 'c': clearButton.click();
                break;

        }
    }
    public void enterNumber(double number)
    {
        click(resultField);
        resultField.sendKeys(String.valueOf(number));
    }

    public int addTwoPositiveIntegersNumbers(int firstNumber, int secondNumber)
    {
        chooseNumberFromKeyboard(firstNumber);
        enterSymbol('+');
        chooseNumberFromKeyboard(secondNumber);
        enterSymbol('=');

        return firstNumber+secondNumber;
    }
    public int addNegativeToPositive(int firstNumber, int secondNumber){
        String FirstNumberAfterMinus = String.valueOf(firstNumber);
        enterSymbol('-');
        chooseNumberFromKeyboard(Integer.parseInt(StringUtils.substringAfter(FirstNumberAfterMinus,"-")));
        enterSymbol('+');
        chooseNumberFromKeyboard(secondNumber);

        enterSymbol('=');

        return -firstNumber+secondNumber;
    }
    public double actionWithTwoNumber(double firstNumber, double secondNumber, char symbol)  {

        enterNumber(firstNumber);
        enterSymbol(symbol);
        enterNumber(secondNumber);
        enterSymbol('=');

        return firstNumber+secondNumber;
    }


    public String getValue()
    {
        return resultField.getAttribute("value");
    }

}
