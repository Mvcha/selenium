package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ButtonsPage extends BasePage {

    @FindBy(xpath = "//button[@id='btn_one']")
    private WebElement firstButton;
    @FindBy(id = "btn_two")
    private WebElement secondButton;
    @FindBy(id = "btn_three")
    private WebElement thirdButton;
    @FindBy(id = "btn_four")
    private WebElement fourthButton;


    public ButtonsPage(WebDriver driver) {
        super(driver);
    }

    public void clickFirstButton()
    {
        firstButton.click();
    }
    public void clickSecondButton()
    {
        JavascriptExecutor javascriptExecutor=(JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();",secondButton);

    }
    public void clickThirdButton()
    {
        actions.moveToElement(thirdButton)
                .click()
                .perform();
    }
    public boolean clickFourthButton()
    {
        if(fourthButton.isEnabled())
        {
            click(fourthButton);
            return true;
        }
        else
            return false;
    }
}
