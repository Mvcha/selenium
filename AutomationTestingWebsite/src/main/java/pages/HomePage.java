package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends BasePage {

    WebDriver driver;
    @FindBy(className = "close-cookie-warning")
    private WebElement closeCookieWarning;
    @FindBy(linkText = "HOMEPAGE")
    private WebElement homePageBtn;
    @FindBy(linkText = "ACCORDION")
    private WebElement accordionBtn;
    @FindBy(linkText = "ACTIONS")
    private WebElement actionsBtn;
    @FindBy(linkText = "BROWSER TABS")
    private WebElement browserTabsBtn;
    @FindBy(linkText = "BUTTONS")
    private WebElement buttonsBtn;
    @FindBy(linkText = "CALCULATOR (JS)")
    private WebElement calculatorBtn;
    @FindBy(linkText = "CONTACT US FORM TEST")
    private WebElement contactUsBtn;
    @FindBy(linkText = "DATE PICKER")
    private WebElement datePickerBtn;
    @FindBy(linkText = "DROPDOWN CHECKBOX RADIO")
    private WebElement dropDownCheckboxRadioBtn;
    @FindBy(linkText = "FILE UPLOAD")
    private WebElement fileUploadBtn;
    @FindBy(linkText = "HIDDEN ELEMENTS")
    private WebElement hiddenElementsBtn;
    @FindBy(linkText = "IFRAMES")
    private WebElement iframesBtn;
    @FindBy(linkText = "LOADER")
    private WebElement loaderBtn;
    @FindBy(linkText = "LOADER TWO")
    private WebElement loaderBtn2;
    @FindBy(linkText = "MOUSE MOVEMENT")
    private WebElement mouseMovementBtn;
    @FindBy(linkText = "LOGIN PORTAL TEST")
    private WebElement loginPortalTestBtn;
    @FindBy(linkText = "POP UPS & ALERTS")
    private WebElement popUPSBtn;
    @FindBy(linkText = "PREDICTIVE SEARCH")
    private WebElement predictiveSearchBtn;
    @FindBy(linkText = "TABLES")
    private WebElement tablesBtn;
    @FindBy(linkText = "TEST STORE")
    private WebElement testStoreBtn;
    @FindBy(linkText = "ABOUT ME")
    private WebElement aboutMeBtn;
    @FindBy(css = ".toggle")
    private WebElement toggle;
    @FindBy(css = "ul li> a")
    private List<WebElement> menuList;

    public HomePage(WebDriver driver) {
        super(driver);
        if(isDisplayed(closeCookieWarning))
        {
            closeCookieWarning.click();
        }
    }


    public void goToAccordion() {
        click(accordionBtn);
    }

    public void goToActions() {
        click(actionsBtn);
    }

    public void goToBrowserTabs() {
        click(browserTabsBtn);
    }

    public void goToButtons() {
        click(buttonsBtn);
    }

    public void goToCalculator() {
        click(calculatorBtn);
    }

    public void goToContactUs() {
        click(contactUsBtn);
    }

    public void goToDatePicker() {
        click(datePickerBtn);
    }

    public void goToDropDownCheckboxPage() {
        click(dropDownCheckboxRadioBtn);
    }

    public void goToFileUploadPage() {
        click(fileUploadBtn);
    }

    public void goToHiddenPageElements() {
        click(hiddenElementsBtn);
    }

    public void goToLoaderPage() {
        click(loaderBtn);
    }
    public void goToLoaderTwoPage() {
        click(loaderBtn2);
    }
    public void goToLoginPortalPage() {
        click(loginPortalTestBtn);
    }
    public void goToPopUpsAndAlertsPage(){click(popUPSBtn);}
    public void goToPredictivePageSearch(){click(predictiveSearchBtn);}
    public void goToTables(){click(tablesBtn);}


    public List<String> getListInLeftMenu()
    {
        List<String> listLeftMenu = new ArrayList<>();

        for (WebElement li: menuList) {
            listLeftMenu.add(li.getText());
        }
        return listLeftMenu;
    }

    public void clickToggleButton()
    {
        click(toggle);
    }

}
