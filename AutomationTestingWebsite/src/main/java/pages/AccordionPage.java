package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class AccordionPage extends BasePage{

    @FindBy (css=".accordion .accordion-header")
    private List<WebElement> accordions;

    @FindBy (css = ".accordion .accordion-header:nth-of-type(1)")
    private WebElement firstAccordion;
    @FindBy (css = ".accordion .accordion-header:nth-of-type(3)")
    private WebElement secondAccordion;
    @FindBy (css = ".accordion .accordion-header:nth-of-type(5)")
    private WebElement thirdAccordion;

    @FindBy (css = ".accordion .accordion-content:nth-of-type(2)")
    private WebElement firstAccordionContent;
    @FindBy (css = ".accordion .accordion-content:nth-of-type(4)")
    private WebElement secondAccordionContent;
    @FindBy (css = ".accordion .accordion-content:nth-of-type(6)")
    private WebElement thirdAccordionContent;

    public AccordionPage(WebDriver driver) {
        super(driver);
    }


    public List<String> getListAccordions(){
        List<String> listAccordions = new ArrayList<>();

        for (WebElement header: accordions) {
            listAccordions.add(header.getText());
        }
        return listAccordions;
    }

    public void closeFirstAccordion()
    {
        click(firstAccordion);
        waitToBeInvisible(firstAccordionContent);
    }
    public void closeSecondAccordion()
    {
        click(secondAccordion);
        waitToBeInvisible(secondAccordionContent);
    }
    public void closeThirdAccordion()
    {
        click(thirdAccordion);
        waitToBeInvisible(thirdAccordionContent);
    }

    public void openFirstAccordion()
    {
        click(firstAccordion);
        waitToBeVisible(firstAccordionContent);
    }
    public void openSecondAccordion()
    {
        click(secondAccordion);
        waitToBeVisible(secondAccordionContent);
    }
    public void openThirdAccordion()
    {
        click(thirdAccordion);
        waitToBeVisible(thirdAccordionContent);
    }
    public String getFirstAccordionContent()
    {
        return firstAccordionContent.getText();
    }
    public String getSecondAccordionContent()
    {
        return secondAccordionContent.getText();
    }
    public String getThirdAccordionContent()
    {
        return thirdAccordionContent.getText();
    }

    public void showClassName()
    {
        System.out.println(firstAccordion.getAttribute("class"));
    }

}
