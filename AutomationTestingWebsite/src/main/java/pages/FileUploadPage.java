package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static java.lang.System.getProperty;

public class FileUploadPage extends BasePage{

    @FindBy(id = "fileToUpload")
    private WebElement fileUpload;
    @FindBy(name = "submit")
    private WebElement submitButton;

    public void fileUpload(){
        fileUpload.sendKeys(getProperty("user.dir")+"\\src\\main\\resources\\file.txt");
        submitButton.submit();
    }
    public FileUploadPage(WebDriver driver) {
        super(driver);
    }
}
