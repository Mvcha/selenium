package pages;

import helpers.Configuration;
import helpers.DriverInstance;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class BasePage {
    public static WebDriver driver;
    public Actions actions;
    public  WebDriverWait wait;

    public static WebDriver getDriver() throws IOException {
        return getDriver();
    }
    public BasePage (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, Duration.ofSeconds(Configuration.getDefaultWaitTime()));
    }

    public void click (WebElement element){
        element.click();
    }
    public void click (By by){
        driver.findElement(by).click();
    }
    public void sendKeys(WebElement element, String text) {
        System.out.println("Typing: " + text);
        element.sendKeys(text);
    }
    public void sendKeysAndClear(WebElement element, String text) {
        element.clear();
        sendKeys(element, text);
    }
    public boolean isDisplayed(By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    public boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void takeScreenShot(String testName) throws IOException {
        TakesScreenshot screenShot =((TakesScreenshot)driver);
        File SourceFile=screenShot.getScreenshotAs(OutputType.FILE);
        File DestinationFile=new File(System.getProperty("user.dir")+"\\target\\reports\\screenshots\\"+testName+".png");
        FileUtils.copyFile(SourceFile, DestinationFile);
    }
    public void waitToBeVisible(By by)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
    public void waitToBeVisible(WebElement element)
    {
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    public void waitToBeInvisible(By by)
    {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }
    public void waitToBeInvisible(WebElement element)
    {
        wait.until(ExpectedConditions.invisibilityOf(element));
    }
    public void waitToVisibilityOfElementWithText(By by, String text){ wait.until(ExpectedConditions.textToBe(by, text));}

}
