package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;

public class DatePickerPage extends BasePage{


    @FindBy(id = "basicDate")
    private WebElement basicDateInput;
    @FindBy(id = "rangeDate")
    private WebElement rangeDateInput;
    @FindBy(css = ".resetDate > .flatpickr-input")
    private WebElement weekDateInput;
    @FindBy(id = "timePicker")
    private WebElement timeInput;

    //First calendar
    @FindBy(css="body [tabindex='-1']:nth-child(11) .dayContainer")
    private List<WebElement> daysFirstCalendar;
    @FindBy(xpath = "//body/div[2]//div[@class='flatpickr-days']/div[@class='dayContainer']" +
            "//span[(@class='flatpickr-day ') or contains(@class, 'flatpickr-day today')]")
    private List<WebElement> daysListInCalendar;
    @FindBy(className = "flatpickr-prev-month")
    private WebElement arrowPreviousMonthInBasicCalendar;
    @FindBy(className = "flatpickr-next-month")
    private WebElement arrowNextMonthInBasicCalendar;
    @FindBy(xpath = "//body/div[2]//span[@title='Scroll to increment']")
    private WebElement titleWithMonthNameFirstCalendar;
    @FindBy(xpath = "//body/div[2]/div[@class='flatpickr-month']//span[@class='arrowUp']")
    private WebElement arrowYearUpBasicCalendar;
    @FindBy(xpath = "//body/div[2]/div[@class='flatpickr-month']//span[@class='arrowDown']")
    private WebElement arrowYearDownBasicCalendar;
    @FindBy(xpath = "//body/div[2]/div[@class='flatpickr-time']/div[1]/input[@title='Scroll to increment']")
    private WebElement hourInputBasicCalendar;
    @FindBy(xpath = "//body/div[2]/div[@class='flatpickr-time']/div[2]/input[@title='Scroll to increment']")
    private WebElement minutesInputBasicCalendar;
    @FindBy(className = "flatpickr-am-pm")
    private WebElement amToggle;

    //Range Calendar
    @FindBy(xpath = "//body/div[3]/div[@class='flatpickr-month']//span[@class='arrowUp']")
    private WebElement arrowYearUpRangeCalendar;
    @FindBy(xpath = "//body/div[3]/div[@class='flatpickr-month']//span[@class='arrowDown']")
    private WebElement arrowYearDownRangeCalendar;
    @FindBy(xpath = "//body/div[3]//span[@class='flatpickr-prev-month']")
    private WebElement arrowPreviousMonthRangeCalendar;
    @FindBy(xpath = "//body/div[3]//span[@class='flatpickr-next-month']")
    private WebElement arrowNextMonthRangeCalendar;

    //Week Calendar

    @FindBy(xpath = "//body/div[5]/div[@class='flatpickr-month']//span[@class='arrowUp']")
    private WebElement arrowYearUpWeekCalendar;
    @FindBy(xpath = "//body/div[5]/div[@class='flatpickr-month']//span[@class='arrowDown']")
    private WebElement arrowYearDownWeekCalendar;
    @FindBy(xpath = "//body/div[5]//span[@class='flatpickr-prev-month']")
    private WebElement arrowPreviousMonthWeekCalendar;
    @FindBy(xpath = "//body/div[5]//span[@class='flatpickr-next-month']")
    private WebElement arrowNextMonthWeekCalendar;
    @FindBy(css = "button[title='clear']")
    private WebElement resetButton;

    //Time picker
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[1]/span[@class='arrowUp']")
    private WebElement arrowHourUpTimeCalendar;
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[1]/span[@class='arrowDown']")
    private WebElement arrowHourDownTimeCalendar;
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[2]/span[@class='arrowUp']")
    private WebElement arrowMinutesUpTimeCalendar;
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[2]/span[@class='arrowDown']")
    private WebElement arrowMinutesDownTimeCalendar;
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[1]/input[@title='Scroll to increment']")
    private WebElement hourInputTimeCalendar;
    @FindBy(xpath = "//body/div[4]/div[@class='flatpickr-time time24hr']/div[2]/input[@title='Scroll to increment']")
    private WebElement minutesInputTimeCalendar;
            


    public void firstCalendar(int day, String monthName) throws InterruptedException {
        basicDateInput.click();
        chooseDayYearMonth(2,monthName, day, arrowYearUpBasicCalendar,arrowYearDownBasicCalendar, arrowNextMonthInBasicCalendar,arrowPreviousMonthInBasicCalendar);
        Thread.sleep(3000);
        String getMonthName=driver.findElement(By.className("cur-month")).getText();
        hourInputBasicCalendar.clear();
        hourInputBasicCalendar.sendKeys("11");
        minutesInputBasicCalendar.clear();
        minutesInputBasicCalendar.sendKeys("42");
        actions.sendKeys(Keys.ESCAPE).perform();
        wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("basicDate"),getMonthName+", "+day+" 2028 23:42" ));
        }

    public void rangeTimeCalendar(int startDay, int endDay, String monthName) throws InterruptedException {
        rangeDateInput.click();
        chooseDayYearMonth(3 , monthName,startDay,arrowYearUpRangeCalendar,arrowYearDownRangeCalendar, arrowNextMonthRangeCalendar, arrowPreviousMonthRangeCalendar);
        chooseDay(endDay, 3);

        wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("rangeDate"),"2028-12-11 to 2028-12-19" ));
    }

    public void withWeekCalendar(int day, String monthName) throws InterruptedException {
        weekDateInput.click();
        chooseDayYearMonth(5,monthName, day,arrowYearUpWeekCalendar,arrowYearDownWeekCalendar, arrowNextMonthWeekCalendar, arrowPreviousMonthWeekCalendar);
        wait.until(ExpectedConditions.textToBePresentInElementValue(weekDateInput,"2028-12-12" ));
    }

    public String timePicker()
    {
        timeInput.click();
        Random random=new Random();
        Calendar calendar=new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE,0);

        int upperClickOnHour= 24;
        int upperClickOnMinutes=30;
        int hourClicks = random.nextInt(upperClickOnHour);
        int minutesClicks=random.nextInt(upperClickOnMinutes);

        calendar.add(Calendar.HOUR_OF_DAY, hourClicks);
        calendar.add(Calendar.MINUTE, (minutesClicks*5));

        for (int i=0; i<hourClicks; i++)
        {
            arrowHourUpTimeCalendar.click();
        }
        for (int i=0; i<minutesClicks; i++)
        {
            arrowMinutesUpTimeCalendar.click();
        }

        actions.sendKeys(Keys.ESCAPE).perform();

        return calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE);
    }
    public void clickReset()
    {
        click(resetButton);
    }


    public DatePickerPage(WebDriver driver) {
        super(driver);
    }



    public void chooseDayYearMonth(int calendarFlag,String monthName, int day, WebElement arrowYearUp, WebElement arrowYearDown, WebElement arrowNextMonth, WebElement arrowPreviousMonth) throws InterruptedException {
        Date date= new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int monthNumber = localDate.getMonthValue();
        Month month= Month.of(monthNumber+1);
        String newMonthName = String.valueOf(month).substring(0, 1).toUpperCase() +String.valueOf(month).substring(1).toLowerCase();

        //arrowYearUp.click();
        //arrowNextMonth.click();
        chooseMonth(monthName, arrowNextMonth,calendarFlag);
        chooseYear(2028, arrowYearUp, arrowYearDown, calendarFlag);
        chooseDay(day,calendarFlag);
        /*try {

            waitToVisibilityOfElementWithText(By.xpath("//body/div["+calendarFlag+"]//span[@title='Scroll to increment']"), monthName);
            Thread.sleep(3000);
            chooseDay(day,calendarFlag);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }*/
    }

    public void chooseDay(int day, int calendarFlag)
    {
        List<WebElement> daysInMonth=driver.findElements(By.xpath("//body/div["+calendarFlag+"]//div[@class='flatpickr-days']/div[@class='dayContainer']//span[contains(@class, 'flatpickr-day')]"));

        for (WebElement element : daysInMonth
        ) {

            if(Integer.parseInt(element.getText())==day)
            {
                element.click();
                break;
            }
        }
    }

    public void chooseMonth(String expectedMonth, WebElement arrowNextMonth, int calendarFlag) throws InterruptedException {
        String initialMonth=driver.findElement(By.xpath("//body/div["+calendarFlag+"]//span[@title='Scroll to increment']")).getText();
        if(!initialMonth.equals(expectedMonth))
        {
            while (!driver.findElement(By.xpath("//body/div["+calendarFlag+"]//span[@title='Scroll to increment']")).getText().equals(expectedMonth))
            {
                arrowNextMonth.click();
                Thread.sleep(500);
            }
        }

    }

    public void chooseYear(int expectedYear, WebElement yearUp, WebElement yearDown, int calendarFlag) throws InterruptedException {

        int actualYear= Integer.parseInt(driver.findElement(By.xpath("//body/div["+calendarFlag+"]/div[@class='flatpickr-month']//input[@title='Scroll to increment']")).getAttribute("value"));
        if(actualYear>expectedYear)
        {
            while (Integer.parseInt(driver.findElement(By.xpath("//body/div["+calendarFlag+"]/div[@class='flatpickr-month']//input[@title='Scroll to increment']")).getAttribute("value"))!=expectedYear)
            {
                yearDown.click();
                Thread.sleep(300);
            }
        }
        if(actualYear<expectedYear)
        {
            while (Integer.parseInt(driver.findElement(By.xpath("//body/div["+calendarFlag+"]/div[@class='flatpickr-month']//input[@title='Scroll to increment']")).getAttribute("value"))!=expectedYear)
            {
                yearUp.click();
                Thread.sleep(300);
            }
        }
    }

}
