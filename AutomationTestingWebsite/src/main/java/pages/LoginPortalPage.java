package pages;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import static java.lang.System.getProperty;

public class LoginPortalPage extends BasePage{

    @FindBy(id = "login_text")
    private WebElement usernameInput;

    @FindBy(id = "login_password")
    private WebElement passwordInput;

    @FindBy(id = "login_btn")
    private WebElement loginButton;


    public XSSFSheet getFileWithCredentials() throws IOException {
        File file=new File(getProperty("user.dir")+"/src/main/resources/credentials.xlsx");
        if(file.exists() && file.canRead()) {
            FileInputStream workbookLocation = new FileInputStream(file);

            XSSFWorkbook workbook = new XSSFWorkbook(workbookLocation);
            XSSFSheet sheet = workbook.getSheetAt(0);
            return sheet;
        }
    return null;
    }

    public String[] enterCredentials(int recordNumber, XSSFSheet sheet)
    {
        Row row=sheet.getRow(recordNumber);
        Cell cellWithLogin = row.getCell(0);
        Cell cellWithPassword = row.getCell(1);
        Cell cellWithResult=row.getCell(2);

        String login = cellWithLogin.toString();
        String password = cellWithPassword.toString();

        usernameInput.sendKeys(login);
        passwordInput.sendKeys(password);
        loginButton.click();


        String alertText = driver.switchTo().alert().getText();
        return new String[]{alertText, cellWithResult.toString()};
    }

    public LoginPortalPage(WebDriver driver) {
        super(driver);
    }
}
