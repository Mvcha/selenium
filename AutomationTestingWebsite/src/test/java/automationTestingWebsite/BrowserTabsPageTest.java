package automationTestingWebsite;

import base.Pages;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class BrowserTabsPageTest extends Pages {

    @Test
    public void openOneNewPage(){
        ExtentManager.log("Testing opening new tabs");

        homePage.goToBrowserTabs();
        ExtentManager.pass("Opened website with tabs");
        browserTabsPage.openNewTab();

        Assert.assertEquals(driver.findElement(By.cssSelector("button#L2AGLb > div[role='none']")).getText(), "Zaakceptuj wszystko");
        ExtentManager.pass("Opened new tab properly");
    }
}
