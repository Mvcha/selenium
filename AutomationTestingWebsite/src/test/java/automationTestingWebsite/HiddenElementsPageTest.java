package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ExtentManager;

public class HiddenElementsPageTest extends Pages {

    @Test
    public void showHiddenText()
    {
        homePage.goToHiddenPageElements();
        ExtentManager.log("Opened hidden page website");
        Assert.assertEquals(hiddenElementsPage.showHiddenParagraph(), "You have displayed the hidden text!", "Checking the visibility of text");
        ExtentManager.pass("Showed hidden text");
    }
}
