package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class PopUpsAndAlertsPageTest extends Pages {

    @Test
    public void popUpWindowsTest(){
        ExtentManager.log("Popup Windows Test");
        homePage.goToPopUpsAndAlertsPage();
        ExtentManager.pass("Popup and alert website opened");
        Assert.assertEquals(popUpsAndAlertsPage.firstTriggerPopUp(), 1, "A new window has been closed");
        ExtentManager.pass("Popup opened and closed");
    }

    @Test
    public void alertTest(){
        ExtentManager.log("Popup and alert test");
        homePage.goToPopUpsAndAlertsPage();
        ExtentManager.pass("Popup and alert website opened");
        popUpsAndAlertsPage.secondTriggerAlert();
        Assert.assertEquals(popUpsAndAlertsPage.secondTriggerAlert(), "You have triggered the alert!", "Alert opened and after closed");
        ExtentManager.pass("Alert opened and closed");
    }
}
