package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class PredictiveSearchPageTest extends Pages {

    @Test
    public void checkingResultsOnList()
    {
        ExtentManager.log("Predictive Search Page Test");
        homePage.goToPredictivePageSearch();
        ExtentManager.pass("Predictive search website opened");
        Assert.assertTrue(predictiveSearchPage.enterLetterAndCheckCorrectList(), "First letter on the list is the same like in input");
        ExtentManager.pass("Checked list");

    }
    @Test
    public void secondTest() {
        ExtentManager.log("Predictive Search Page Test");
        homePage.goToPredictivePageSearch();
        ExtentManager.pass("Predictive search website opened");

        Assert.assertTrue(predictiveSearchPage.checkingVisibilityChosenElement(), "Checking chosen data and what is displayed");
        ExtentManager.pass("Checked visibility element");
    }
}
