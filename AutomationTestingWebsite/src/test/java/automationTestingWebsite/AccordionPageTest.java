package automationTestingWebsite;

import base.Pages;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.testng.Assert;
import org.testng.ITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;


@Listeners(TestListener.class)
public class AccordionPageTest extends Pages {

public static String firstAccordionContext = "Testing is a repetitive process. The slightest modification in code must be tested to ensure that the software is providing the desired functionality and result. Repeating tests manually is a time consuming and costly process. Automated tests can be run repetitively at no additional costs. Selenium is a highly portable tool that runs on multiple platforms as well as browsers. It therefore allows automation engineers the ease of writing code without worrying about the platform on which it will run.";
public String secondAccordionContext = "Software is written in a number of languages. One of the challenges faced by automated testers is integrating the automation tools with the development environment for CI. With Selenium bindings for Java, .NET, Ruby, Perl, Python, PHP, Groovy and JavaScript, it is very easy to integrate with the development environment.";
public String thirdAccordionContext = "The remote control server of Selenium allows automation testers to create a test infrastructure that is spread across multiple locations (including cloud) to drive the scripts on a large set of browsers.";

    @Test
    public void accordionTest(){
        ExtentManager.log("Testing accordions");
        homePage.goToAccordion();
        ExtentManager.pass("Opened accordion page");
        accordionPage.openFirstAccordion();
        Assert.assertEquals(accordionPage.getFirstAccordionContent(), firstAccordionContext, "Compare between actual and expected first accordion context");
        ExtentManager.pass("Compare between actual and expected first accordion context");
        accordionPage.closeFirstAccordion();

        accordionPage.openSecondAccordion();
        Assert.assertEquals(accordionPage.getSecondAccordionContent(), secondAccordionContext, "Compare between actual and expected second accordion context");
        ExtentManager.pass("Compare between actual and expected second accordion context");
        accordionPage.closeSecondAccordion();

        accordionPage.openThirdAccordion();
        Assert.assertEquals(accordionPage.getThirdAccordionContent(), thirdAccordionContext, "Compare between actual and expected third accordion context");
        ExtentManager.pass("Compare between actual and expected third accordion context");
        accordionPage.closeThirdAccordion();
    }


}
