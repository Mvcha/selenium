package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class ContactUsPageTest extends Pages {
    String firstName = "John";
    String lastName="Miller";
    String email="j.miller@gmail.com";
    String comment="Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    String expectedUrl = "https://automationtesting.co.uk/contact-form-thank-you.html";

    @Test
    public void fillingAndClearingForm(){

        ExtentManager.log("Testing filling form");

        homePage.goToContactUs();
        ExtentManager.pass("Opened contact website");

        contactUsPage.fillForm(firstName, lastName,email,comment);
        ExtentManager.pass("Form is filled");

        Assert.assertEquals(contactUsPage.getFirstName(), firstName, "First name in form is correct");
        ExtentManager.pass("First name is correct");
        Assert.assertEquals(contactUsPage.getLastName(), lastName, "Last name in form is correct");
        ExtentManager.pass("Last name is correct");
        Assert.assertEquals(contactUsPage.getEmail(), email, "Email in form is correct");
        ExtentManager.pass("Email is correct");
        Assert.assertEquals(contactUsPage.getComment(), comment, "Message in form is correct");
        ExtentManager.pass("Message is correct");

        ExtentManager.log("Clearing form");
        contactUsPage.clearForm();
        Assert.assertEquals(contactUsPage.getFirstName(), "", "First name in form should be empty");
        ExtentManager.pass("First name is cleared");
        Assert.assertEquals(contactUsPage.getLastName(), "", "Last name in form should be empty");
        ExtentManager.pass("Last name is cleared");
        Assert.assertEquals(contactUsPage.getEmail(), "", "Email in form should be empty");
        ExtentManager.pass("Email is cleared");
        Assert.assertEquals(contactUsPage.getComment(), "", "Message in form should be empty");
        ExtentManager.pass("Message is cleared");


    }

    @Test
    public void fillAndSubmitForm(){

        ExtentManager.log("Testing filling form");

        homePage.goToContactUs();
        ExtentManager.pass("Opened contact website");

        contactUsPage.fillForm(firstName, lastName,email,comment);
        ExtentManager.pass("Form is filled");

        contactUsPage.submitForm();

        String actualUrl= driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, expectedUrl, "Opened website after submit form");
        ExtentManager.pass("Opened website after submit form");
    }
}
