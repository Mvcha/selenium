package automationTestingWebsite;


import base.Pages;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class DropDownCheckboxPageTest extends Pages {


    @Test
    public void dropDownCheckBoxTest() {
        ExtentManager.log("Drop down, checkboxes testing");
        homePage.goToDropDownCheckboxPage();
        ExtentManager.pass("Drop down website opened");
        checkRadioButtons();
        checkBoxes();
        navigationMenu();
        dropdownMenu();
    }

    public void checkRadioButtons() {

        dropDownCheckboxPage.clickRadioButton(1);
        dropDownCheckboxPage.clickRadioButton(2);
        dropDownCheckboxPage.clickRadioButton(3);
        ExtentManager.pass("Radio buttons checked");
    }

    public void checkBoxes() {

        dropDownCheckboxPage.clickCheckbox(1);
        dropDownCheckboxPage.clickCheckbox(2);
        dropDownCheckboxPage.clickCheckbox(3);

        ExtentManager.pass("Checkboxes checked");
    }

    public void navigationMenu() {
        homePage.goToDropDownCheckboxPage();
        dropDownCheckboxPage.chooseAnimalFromMenu("Mouse");
        Assert.assertEquals(driver.findElement(By.id("outputMessage")).getText(), "You clicked on menu option 'Mouse'", "Verification of the selected element");
        dropDownCheckboxPage.chooseAnimalFromMenu("Cat");
        Assert.assertEquals(driver.findElement(By.id("outputMessage")).getText(), "You clicked on menu option 'Cat'", "Verification of the selected element");
        dropDownCheckboxPage.chooseAnimalFromMenu("Fish");
        Assert.assertEquals(driver.findElement(By.id("outputMessage")).getText(), "You clicked on menu option 'Fish'", "Verification of the selected element");
        dropDownCheckboxPage.chooseAnimalFromMenu("Dog");
        Assert.assertEquals(driver.findElement(By.id("outputMessage")).getText(), "You clicked on menu option 'Dog'", "Verification of the selected element");

        ExtentManager.pass("Navigation menu checked");
    }

    public void dropdownMenu() {
        homePage.goToDropDownCheckboxPage();
        Assert.assertEquals(dropDownCheckboxPage.chooseCarFromDropdown("BMW"), "BMW", "Verification of the selected element");
        Assert.assertEquals(dropDownCheckboxPage.chooseCarFromDropdown("Ford"), "Ford", "Verification of the selected element");
        Assert.assertEquals(dropDownCheckboxPage.chooseCarFromDropdown("Honda"), "Honda", "Verification of the selected element");
        Assert.assertEquals(dropDownCheckboxPage.chooseCarFromDropdown("Jeep"), "Jeep", "Verification of the selected element");
        Assert.assertEquals(dropDownCheckboxPage.chooseCarFromDropdown("Suzuki"), "Suzuki", "Verification of the selected element");

        ExtentManager.pass("Dropdown checked");
    }
}
