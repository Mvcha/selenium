package automationTestingWebsite;

import base.Pages;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class ActionsPageTest extends Pages {

    @Test
    public void actionsTests() {
        ExtentManager.log("Testing actions");
        homePage.goToActions();
        ExtentManager.pass("Opened actions page");
        actionsPage.clickAndHold();
        Assert.assertEquals(driver.findElement(By.id("click-box")).getText(), "Keep holding down!", "Compare expected and real text in rectangle with click and hold");
        ExtentManager.pass("Click and Hold work properly");

        actionsPage.doubleClick();
        Assert.assertEquals(driver.findElement(By.id("doubClickStartText")).getText(), "Well Done!", "Compare expected and real text in rectangle with double click");
        ExtentManager.pass("Double click works properly");

        actionsPage.shiftAndClick();
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(), "The SHIFT key was pressed!", "Compare expected and real alert info");
        ExtentManager.pass("Shift and click work properly");

    }

}
