package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;


@Listeners(TestListener.class)
public class CalculatorPageTest extends Pages {


    @Test
    public void testsCalculator() throws InterruptedException {
        ExtentManager.log("Testing Calendar");
        homePage.goToCalculator();
        ExtentManager.pass("Opened website with calculator");

        checkAllNumbersOnKeyboard();
        calculatorPage.enterSymbol('c');

        addTwoPositiveIntegers();
        calculatorPage.enterSymbol('c');

        addNegativeToPositive();
        calculatorPage.enterSymbol('c');

        addTwoNumbersWithDots();
        calculatorPage.enterSymbol('c');

        subtractionTwoNumbers();
        calculatorPage.enterSymbol('c');

        multiplicationTwoNumbers();
        calculatorPage.enterSymbol('c');

        divisionTwoNumbers();
        calculatorPage.enterSymbol('c');

        clearing();
        calculatorPage.enterSymbol('c');

    }

    public void checkAllNumbersOnKeyboard()
    {
        for (int i=0; i<10; i++)
        {
            calculatorPage.chooseNumberFromKeyboard(i);
        }

        Assert.assertEquals(calculatorPage.getValue(), "0123456789", "Check all digits on keyboard");
        ExtentManager.pass("Checked all digits on keyboard");


    }

    public void addTwoPositiveIntegers()
    {
        calculatorPage.addTwoPositiveIntegersNumbers(8,2);

        Assert.assertEquals(calculatorPage.getValue(), "10", "Check adding two positives integers");
        ExtentManager.pass("Added two positives integers");
    }

    public void addNegativeToPositive(){
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(-8,2,'+');

        Assert.assertEquals(calculatorPage.getValue(), "-6", "Check adding negative to positive integers from keyboard");
        ExtentManager.pass("Added negative to positive integers");
    }


    public void addTwoNumbersWithDots() {
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(8.25,2.71, '+');

        Assert.assertEquals(calculatorPage.getValue(), "10.96", "Check adding negative to positive integers");
        ExtentManager.pass("Added two numbers with dots");
    }


    public void subtractionTwoNumbers() {
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(-4,6, '-');

        Assert.assertEquals(calculatorPage.getValue(), "-10", "Check subtracting two numbers ");
        ExtentManager.pass("Subtraction two numbers");
    }

    public void multiplicationTwoNumbers() {
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(20,6, '*');

        Assert.assertEquals(calculatorPage.getValue(), "120", "Check multiplication two positive numbers ");
        ExtentManager.pass("Multiplication two numbers");
    }

    public void divisionTwoNumbers() {
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(117649,7, '/');

        Assert.assertEquals(calculatorPage.getValue(), "16807", "Check division two positive numbers ");
        ExtentManager.pass("Division two numbers");
    }


    public void clearing() {
        homePage.goToCalculator();
        calculatorPage.actionWithTwoNumber(117649,7, '/');
        calculatorPage.enterSymbol('c');
        Assert.assertEquals(calculatorPage.getValue(), "", "Clear information on calculator screen ");
        ExtentManager.pass("Clearing");
    }








}
