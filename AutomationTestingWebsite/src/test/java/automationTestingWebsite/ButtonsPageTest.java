package automationTestingWebsite;

import base.Pages;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class ButtonsPageTest extends Pages {

    @Test
    public void buttonsChecking(){

        ExtentManager.log("Testing buttons");
        homePage.goToButtons();
        ExtentManager.pass("Opened buttons website test");
        buttonsPage.clickFirstButton();
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(), "You clicked the first button!", "Check first button");
        ExtentManager.pass("First alert was opened and text is correct");
        alert.accept();
        ExtentManager.pass("First alert prompt was confirmed");


        buttonsPage.clickSecondButton();
        alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(), "You clicked the second button!", "Check second button");
        ExtentManager.pass("Second alert was opened and text is correct");
        alert.accept();
        ExtentManager.pass("Second alert prompt was confirmed");

        buttonsPage.clickThirdButton();
        alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(), "You clicked the third button!", "Check third button");
        ExtentManager.pass("Third alert was opened and text is correct");
        alert.accept();
        ExtentManager.pass("Third alert prompt was confirmed");


        if(buttonsPage.clickFourthButton()==true)
        {
            alert = driver.switchTo().alert();
            Assert.assertEquals(alert.getText(), "You clicked the fourth button!", "Check fourth button");
            ExtentManager.pass("Fourth alert was opened and text is correct");
            alert.accept();
            ExtentManager.pass("Fourth alert prompt was confirmed");

        }
    }

}
