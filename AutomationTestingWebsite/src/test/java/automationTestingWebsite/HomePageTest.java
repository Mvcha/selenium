package automationTestingWebsite;

import base.Pages;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class HomePageTest extends Pages {


    @Test
    public void verifyLeftMenu(){
        String listMenu = "HOMEPAGE,ACCORDION,ACTIONS,BROWSER TABS,BUTTONS,CALCULATOR (JS),CONTACT US FORM TEST,DATE PICKER,DROPDOWN CHECKBOX RADIO,FILE UPLOAD,HIDDEN ELEMENTS,IFRAMES,LOADER,LOADER TWO,LOGIN PORTAL TEST,MOUSE MOVEMENT,POP UPS & ALERTS,PREDICTIVE SEARCH,TABLES,TEST STORE,ABOUT ME";
        ArrayList <String> expectedMenu = new ArrayList<>(Arrays.asList(listMenu.split(",")));
        Assert.assertEquals(homePage.getListInLeftMenu(), expectedMenu, "Compare real left menu list with expected");

    }
    @Test
    public void verifyToggleButton(){
        homePage.clickToggleButton();
        homePage.waitToBeInvisible(By.linkText("HOMEPAGE"));
        Assert.assertEquals(homePage.isDisplayed(By.linkText("HOMEPAGE")), false, "Left menu should be hidden");

        homePage.clickToggleButton();
        homePage.waitToBeVisible(By.linkText("HOMEPAGE"));
        Assert.assertEquals(homePage.isDisplayed(By.linkText("HOMEPAGE")), true, "Left menu should be visible");
    }
}
