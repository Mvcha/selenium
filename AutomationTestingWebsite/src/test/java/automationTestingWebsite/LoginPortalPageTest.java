package automationTestingWebsite;

import base.Pages;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

import java.awt.print.PageFormat;
import java.io.IOException;

@Listeners(TestListener.class)
public class LoginPortalPageTest extends Pages {


    @Test
    public void checkCorrectCredentials() throws IOException{
        ExtentManager.log("Login portal test");

        homePage.goToLoginPortalPage();
        ExtentManager.pass("Login portal page opened");
        XSSFSheet sheet=loginPortalPage.getFileWithCredentials();
        String [] results=loginPortalPage.enterCredentials(1, sheet);

        if(results[1].equals("Yes"))
        {
            Assert.assertEquals(results[0], "validation succeeded", "Checking login form with correct credentials");
            ExtentManager.pass("Validation successfully");
        }
        else
        {
            Assert.assertEquals(results[0], "validation failed", "Checking login form with incorrect credentials");
            ExtentManager.fail("Validation failed");
        }

    }

    @Test
    public void checkIncorrectCredentials() throws IOException{
        ExtentManager.log("Login portal test");
        homePage.goToLoginPortalPage();
        ExtentManager.pass("Login portal page opened");
        XSSFSheet sheet=loginPortalPage.getFileWithCredentials();
        String [] results=loginPortalPage.enterCredentials(2, sheet);

        if(results[1].equals("Yes"))
        {
            Assert.assertEquals(results[0], "validation succeeded", "Checking login form with correct credentials");
            ExtentManager.pass("Validation successfully");
        }
        else
        {
            Assert.assertEquals(results[0], "validation failed", "Checking login form with incorrect credentials");
            ExtentManager.fail("Validation failed");
        }

    }

}
