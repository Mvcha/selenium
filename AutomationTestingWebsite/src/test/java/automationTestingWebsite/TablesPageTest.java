package automationTestingWebsite;

import base.Pages;
import models.TableData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

import java.util.ArrayList;
import java.util.List;


@Listeners(TestListener.class)
public class TablesPageTest extends Pages {


    @Test
    public void sortByFirstName() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");
        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("firstName", false);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sort by first name");
        ExtentManager.pass("Sorting by first name correct");

    }

    @Test
    public void sortByLastName() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");

        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("lastName", true);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sorting by last name");
        ExtentManager.pass("Sorting by last name correct");
    }

    @Test
    public void sortByDateOfBirth() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");

        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("dateOfBirth", true);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sorting by day of birth");
        ExtentManager.pass("Sorting by date of birth name correct");
    }

    @Test
    public void sortByEmail() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");

        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("email", true);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sorting by email");
        ExtentManager.pass("Sorting by email name correct");
    }

    @Test
    public void sortByResidency() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");

        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("residency", true);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sorting by residency");
        ExtentManager.pass("Sorting by residency name correct");
    }

    @Test
    public void sortByOccupation() {
        ExtentManager.log("Table page test");
        List<TableData> expectedResultList;
        homePage.goToTables();
        ExtentManager.pass("Table page test opened");

        tablesPage.addToModel();
        expectedResultList = tablesPage.sortBy("occupation", false);

        Assert.assertEquals(tablesPage.actualDataFromTableAsString(), expectedResultList.toString(), "Checking sorting by occupation");
        ExtentManager.pass("Sorting by occupation name correct");
    }
}
