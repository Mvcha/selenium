package automationTestingWebsite;

import base.Pages;
import org.testng.annotations.Test;
import utils.ExtentManager;

public class FileUploadPageTest extends Pages {

    @Test
    public void fileUploadTest() {
        homePage.goToFileUploadPage();
        ExtentManager.log("Opened file upload page");
        fileUploadPage.fileUpload();
        ExtentManager.pass("Uploaded file successfully");
    }
}
