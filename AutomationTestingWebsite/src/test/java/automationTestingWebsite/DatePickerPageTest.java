package automationTestingWebsite;

import base.Pages;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ExtentManager;

public class DatePickerPageTest extends Pages {

    private static int day=12;
    private static int startDay=11;
    private static int endDay=19;
    private static String month="December";

    @Test
    public void basicCalendarTest() throws InterruptedException {
        homePage.goToDatePicker();
        ExtentManager.log("Testing Calendar");
        datePickerPage.firstCalendar(day, month);

        Assert.assertEquals(driver.findElement(By.id("basicDate")).getAttribute("value"), month+", "+day+" 2028 23:42","Compare actual and expected date and hour");
    }

    @Test
    public void rangeDateCalendar() throws InterruptedException {
        homePage.goToDatePicker();
        datePickerPage.rangeTimeCalendar(startDay, endDay, month);


        Assert.assertEquals(driver.findElement(By.id("rangeDate")).getAttribute("value"), "2028-12-"+startDay+" to 2028-12-"+endDay, "Compare actual and expected range date");
    }

    @Test
    public void weekDateCalendar() throws InterruptedException {
        homePage.goToDatePicker();
        datePickerPage.withWeekCalendar(day, month);

        Assert.assertEquals(driver.findElement(By.cssSelector(".resetDate > .flatpickr-input")).getAttribute("value"), "2028-12-12","Compare actual and expected date");
        datePickerPage.clickReset();
        Assert.assertEquals(driver.findElement(By.cssSelector(".resetDate > .flatpickr-input")).getAttribute("value"), "","Reset button works properly");

    }

    @Test
    public void timePicker(){
        homePage.goToDatePicker();
        String expectedTime= datePickerPage.timePicker();

        Assert.assertEquals(driver.findElement(By.id("timePicker")).getAttribute("value"), expectedTime,"Compare actual and expected time");
    }
}
