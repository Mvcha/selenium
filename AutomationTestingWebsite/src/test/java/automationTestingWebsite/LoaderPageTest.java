package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ExtentManager;

public class LoaderPageTest extends Pages {

    @Test
    public void waitForLoader()
    {
        ExtentManager.log("Loader waiting test");
        homePage.goToLoaderPage();
        ExtentManager.pass("Opened loader website");
        Assert.assertEquals(loaderPage.clickMeAfterLoader(), "And you have clicked the button!", "Waiting to load and show text after click");
        ExtentManager.pass("Clicked after loader successfully");

    }
}
