package automationTestingWebsite;

import base.Pages;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentManager;
import utils.TestListener;

@Listeners(TestListener.class)
public class LoaderTwoPageTest extends Pages {

    @Test
    public void waitForSecondLoader()
    {
        ExtentManager.log("Second loader testing");
        homePage.goToLoaderTwoPage();
        ExtentManager.pass("Second loader opened");
        Assert.assertEquals(loaderTwoPage.clickMeAfterLoader(), "This is a new paragraph that appears after 8 seconds.", "Waiting to load and show text after click");
        ExtentManager.pass("Loader two successfully");
    }
}
