package base;


import helpers.Configuration;
import helpers.DriverInstance;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;


public class TestBase {

    public WebDriver driver;

    @BeforeMethod
    public void setUp()
    {
        driver=new DriverInstance().getChromeDriver();
        driver.get(Configuration.getAppUrl());
    }
    @AfterMethod
    public void tearDown() {
        driver.quit();
    }


}
