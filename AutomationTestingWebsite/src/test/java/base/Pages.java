package base;

import org.testng.annotations.BeforeMethod;
import pages.*;

public class Pages extends TestBase{
    public HomePage homePage;
    public AccordionPage accordionPage;
    public ActionsPage actionsPage;
    public BrowserTabsPage browserTabsPage;
    public ButtonsPage buttonsPage;
    public CalculatorPage calculatorPage;
    public ContactUsPage contactUsPage;
    public DatePickerPage datePickerPage;
    public DropDownCheckboxPage dropDownCheckboxPage;
    public FileUploadPage fileUploadPage;
    public HiddenElementsPage hiddenElementsPage;
    public LoaderPage loaderPage;
    public LoaderTwoPage loaderTwoPage;
    public LoginPortalPage loginPortalPage;
    public PopUpsAndAlertsPage popUpsAndAlertsPage;
    public PredictiveSearchPage predictiveSearchPage;
    public TablesPage tablesPage;
    @BeforeMethod
    public void setupPages()
    {
        homePage=new HomePage(driver);
        accordionPage = new AccordionPage(driver);
        actionsPage = new ActionsPage(driver);
        browserTabsPage = new BrowserTabsPage(driver);
        buttonsPage = new ButtonsPage(driver);
        calculatorPage = new CalculatorPage(driver);
        contactUsPage = new ContactUsPage(driver);
        datePickerPage = new DatePickerPage(driver);
        dropDownCheckboxPage = new DropDownCheckboxPage(driver);
        fileUploadPage = new FileUploadPage(driver);
        hiddenElementsPage = new HiddenElementsPage(driver);
        loaderPage = new LoaderPage(driver);
        loaderTwoPage = new LoaderTwoPage(driver);
        loginPortalPage = new LoginPortalPage(driver);
        popUpsAndAlertsPage = new PopUpsAndAlertsPage(driver);
        predictiveSearchPage = new PredictiveSearchPage(driver);
        tablesPage = new TablesPage(driver);
    }

}
