package pageObjects;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class HomePage extends BasePage {
    public WebDriver driver;

    By cookie = By.className("close-cookie-warning");
    By toggle = By.cssSelector(".toggle");
    By homePageLink = By.linkText("HOMEPAGE");
    By accordionPageLink = By.linkText("ACCORDION");
    By actionsPageLink = By.linkText("ACTIONS");
    By browserTabsPageLink = By.linkText("BROWSER TABS");
    By buttonsPageLink = By.linkText("BUTTONS");
    By calculatorPageLink = By.linkText("CALCULATOR (JS)");
    By contactUsFormTestPageLink = By.linkText("CONTACT US FORM TEST");
    By datePickerPageLink = By.linkText("DATE PICKER");
    By dropdownPageLink = By.linkText("DROPDOWN CHECKBOX & RADIO");
    By fileUploadPageLink = By.linkText("FILE UPLOAD");
    By hiddenElementsPageLink = By.linkText("HIDDEN ELEMENTS");
    By iFramesPageLink = By.linkText("IFRAME");
    By loaderPageLink = By.linkText("LOADER");
    By loginPortalPageLink = By.linkText("LOGIN PORTAL TEST");
    By mousePageLink = By.linkText("MOUSE MOVEMENT");
    By popupPageLink = By.linkText("POP UPS & ALERTS");
    By predictivePageLink = By.linkText("PREDICTIVE SEARCH");
    By tablesPageLink = By.linkText("TABLES");
    By testStorePageLink = By.linkText("TEST STORE");
    By aboutMePageLink = By.linkText("ABOUT ME");

    public HomePage() throws IOException {
        super();

    }

    public WebElement getToggle() throws IOException {
        this.driver = getDriver();
        return driver.findElement(toggle);
    }

    public WebElement getHomePageLink()  throws IOException{
        this.driver = getDriver();
        return driver.findElement(homePageLink);
    }
    public WebElement getAccordionLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(accordionPageLink);
    }

    public  WebElement getCookiePopup() throws IOException{
        this.driver = getDriver();
        return driver.findElement(cookie);
    }
    public WebElement getBrowserTabLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(browserTabsPageLink);
    }

    public WebElement getButtonLink() throws IOException{

        this.driver = getDriver();
        return driver.findElement(buttonsPageLink);
    }

    public WebElement getCalcLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(calculatorPageLink);
    }

    public WebElement getContactUsLink()throws IOException {

        this.driver = getDriver();
        return driver.findElement(contactUsFormTestPageLink);
    }

    public WebElement getDatePickerLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(datePickerPageLink);
    }

    public WebElement getDropdownLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(dropdownPageLink);
    }

    public WebElement getFileUploadLink()throws IOException {
        this.driver = getDriver();
        return driver.findElement(fileUploadPageLink);
    }

    public WebElement getHiddenElementsLink()throws IOException {
        this.driver = getDriver();
        return driver.findElement(hiddenElementsPageLink);
    }

    public WebElement getIframeLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(iFramesPageLink);
    }

    public WebElement getLoaderLink() throws IOException {
        this.driver = getDriver();
        return driver.findElement(loaderPageLink);
    }

    public WebElement getLoginPortalLink()throws IOException {
        this.driver = getDriver();
        return driver.findElement(loginPortalPageLink);
    }

    public WebElement getMouseLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(mousePageLink);
    }

    public WebElement getPopupLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(popupPageLink);
    }

    public WebElement getPredictiveLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(predictivePageLink);
    }

    public WebElement getTablesLink() throws IOException {
        this.driver = getDriver();
        return driver.findElement(tablesPageLink);
    }

    public WebElement getTestStoreLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(testStorePageLink);
    }

    public WebElement getAboutMeLink() throws IOException{
        this.driver = getDriver();
        return driver.findElement(aboutMePageLink);
    }
}
