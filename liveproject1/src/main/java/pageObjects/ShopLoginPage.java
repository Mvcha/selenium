package pageObjects;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class ShopLoginPage extends BasePage {
    public WebDriver driver;


    By emailInput = By.cssSelector("section input[name='email']");
    By passwordInput=By.cssSelector("input[name='password']");
    By signInButton = By.cssSelector("button#submit-login");

    public ShopLoginPage() throws IOException {
        super();
    }


    public WebElement getEmail() throws IOException {
        this.driver=getDriver();
        return driver.findElement(emailInput);
    }

    public WebElement getPassword() throws IOException {
        this.driver=getDriver();
        return driver.findElement(passwordInput);
    }
    public WebElement getSignInButton() throws IOException {
        this.driver=getDriver();
        return driver.findElement(signInButton);
    }
}
