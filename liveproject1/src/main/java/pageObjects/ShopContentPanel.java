package pageObjects;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class ShopContentPanel extends BasePage {
    WebDriver driver;

    By continueShoppingButton = By.xpath("//button[contains(text(), \"Continue\")]");
    By checkOutButton = By.cssSelector(".cart-content-btn .btn-primary");

    public ShopContentPanel() throws IOException {
        super();
    }

    public WebElement getContinueShoppingButton() throws IOException {
        this.driver = getDriver();
        return driver.findElement(continueShoppingButton);
    }

    public WebElement getCheckOutButton () throws IOException {
        this.driver = getDriver();
        return driver.findElement(checkOutButton);
    }
}
