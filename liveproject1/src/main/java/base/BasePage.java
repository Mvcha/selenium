package base;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BasePage {
    public static WebDriver driver;
    private String url;
    private Properties properties;
    public static String screenShotDestinationPath;
    private static String screenShotName;
    private static WebDriverWait wait;

    public BasePage() throws IOException{
        properties=new Properties();

        FileInputStream data = new FileInputStream(
                System.getProperty("user.dir")+"\\src\\main\\resources\\config.properties");
        properties.load(data);
        wait = new WebDriverWait(getDriver(), Duration.ofSeconds(15));
    }
    public static WebDriver getDriver() throws IOException {
       return WebDriverInstance.getDriver();
    }

    public String getUrl () throws IOException
    {
        url=properties.getProperty("url");

        return url;
    }

    public static String takeSnapShot(String name) throws IOException {
        File srcFile= ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        screenShotName = timestamp()+".png";
        String destFile= System.getProperty("user.dir")+"\\report\\screenshots\\"+screenShotName;
        screenShotDestinationPath=destFile;
        try {
            FileUtils.copyFile(srcFile, new File(destFile));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return name;
    }

    public static String timestamp()
    {
        return new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());
    }
    public static String getScreenShotDestinationPath(){
        return screenShotDestinationPath;
    }
    public static String getScreenShotName(){
        return screenShotName;
    }
    public static void waitForElementInvisible(WebElement element) throws IOException {
        wait.until(ExpectedConditions.invisibilityOf(element));
    }
    public static void waitToBeVisible(WebElement element) throws IOException {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}