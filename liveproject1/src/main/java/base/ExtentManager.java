package base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentManager extends BasePage{

    public  static ExtentReports extentReports;
    public static String extentReportPrefix;

    public static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();
    public ExtentManager() throws IOException {
        super();
    }
    public static  ExtentReports getExtentReports(){
        if(extentReports == null){
            setupExtentReport("Live Project 1");
        }
        return extentReports;
    }

    public static ExtentReports setupExtentReport(String testName){
        extentReports = new ExtentReports();
        ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter(System.getProperty("user.dir")+"/report/"+
                extentReportsPrefixName(testName)+".html");
        extentReports.attachReporter(extentSparkReporter);

        extentReports.setSystemInfo("Test", "Name0");
        extentSparkReporter.config().setReportName("Regression Test");
        extentSparkReporter.config().setDocumentTitle("Test Results");
        extentSparkReporter.config().setTheme(Theme.DARK);
        
        return extentReports;
    }

    public static String extentReportsPrefixName(String testName){
        String date= new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(new Date());
        extentReportPrefix = testName+"_"+date;
        return extentReportPrefix;
    }

    public static void flushReport(){
        extentReports. flush();
    }
    
    public synchronized static ExtentTest getTest(){
        return extentTest.get();
    }

    public synchronized static ExtentTest createTest(String name, String description){
        ExtentTest test= extentReports.createTest(name,description);
        extentTest.set(test);
        return getTest();
    }
    public synchronized static void log(String message){
        getTest().info(message);
    }

    public synchronized static void pass(String message){
        getTest().pass(message);
    }
    public synchronized static void fail(String message){
        getTest().fail(message);
    }
    public synchronized static void attachImage(){
        System.out.println(getScreenShotDestinationPath());
        System.out.println(getScreenShotName());
        getTest().addScreenCaptureFromPath("screenshots\\"+getScreenShotName());
        //System.out.println("Working Directory = " + System.getProperty("user.dir"));
    }
}
