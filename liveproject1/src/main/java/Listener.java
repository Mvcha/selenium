import base.ExtentManager;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;


public class Listener extends base.BasePage implements ITestListener {

    public Listener() throws IOException {
        super();
    }

    public synchronized void onStart(ITestContext context){
        ExtentManager.getExtentReports();
        ExtentManager.createTest(context.getName(), context.getName());

    }

    public synchronized void onTestFailure(ITestResult result) {
        ExtentManager.getTest().fail(result.getThrowable());
        try {
            System.out.println("Test failed: "+result.getName());
            takeSnapShot(result.getMethod().getMethodName());
            ExtentManager.attachImage();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public synchronized void onFinish(ITestContext context){
        ExtentManager.getExtentReports();
        ExtentManager.flushReport();
    }

}
