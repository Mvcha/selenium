import base.BasePage;
import base.ExtentManager;
import base.Hooks;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.*;

import java.io.IOException;
import java.time.Duration;

@Listeners(Listener.class)

public class AddRemoveItemBasketTest extends Hooks {

    public AddRemoveItemBasketTest() throws IOException {
        super();
    }

    @Test
    public void addRemoveItem() throws IOException, InterruptedException {

        ExtentManager.log("Starting AddRemoveItemBasketTest");

        HomePage homePage=new HomePage();
        if(homePage.getCookiePopup().isDisplayed())
        {
            homePage.getCookiePopup().click();
        }
        homePage.getTestStoreLink().click();

        ShopHomePage shopHomePage=new ShopHomePage();
        ExtentManager.pass("Reached the shop homepage");

        shopHomePage.getProductOne().click();

        ShopProductPage shopProductPage = new ShopProductPage();
        ExtentManager.pass("Reached the shop product page");
        Select option = new Select(shopProductPage.getSizeOption());
        ExtentManager.pass("Successfully selected product size");
        option.selectByVisibleText("M");
        shopProductPage.getQuantityIncrease().click();
        ExtentManager.pass("Successfully increased product quantity");
        shopProductPage.getAddToCartButton().click();
        ExtentManager.pass("Successfully added product to cart");

        Thread.sleep(2000);
        ShopContentPanel shopContentPanel=new ShopContentPanel();
        ExtentManager.pass("Reached the shop content panel");
        shopContentPanel.getContinueShoppingButton().click();
        shopProductPage.getHomePageLink().click();
        shopHomePage.getProductTwo().click();
        shopProductPage.getAddToCartButton().click();
        shopContentPanel.getCheckOutButton().click();

        ShoppingCart shoppingCart= new ShoppingCart();
        ExtentManager.pass("Reached the shopping cart");
        shoppingCart.getDeleteItemTwo().click();
        waitForElementInvisible(shoppingCart.getDeleteItemTwo());
        System.out.println(shoppingCart.getTotalValue().getText());

        try{
            Assert.assertEquals(shoppingCart.getTotalValue().getText(), "$45.24", "Comparison value with expected value");
            ExtentManager.pass("Total amount matches with the expected amount");
        }
        catch (AssertionError e){
            Assert.fail("Expected value isn't the same like real amount"+shoppingCart.getTotalValue().getText()+ "expected $45.24");
            ExtentManager.fail("Expected value isn't the same like real amount");
        }

    }
}
