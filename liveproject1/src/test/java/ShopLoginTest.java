import base.ExtentManager;
import base.Hooks;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Listeners(Listener.class)

public class ShopLoginTest extends Hooks {

    public ShopLoginTest() throws IOException {
        super();
    }

    @Test
    public void logIn() throws IOException, InterruptedException {


        ExtentManager.log("Starting Shop Login Page");

        HomePage homePage=new HomePage();
        if(homePage.getCookiePopup().isDisplayed())
        {
            homePage.getCookiePopup().click();
        }
        homePage.getTestStoreLink().click();

        ShopHomePage shopHomePage=new ShopHomePage();
        shopHomePage.getLoginButton().click();

        File file=new File(System.getProperty("user.dir")
                +"\\src\\main\\resources\\credentials.xlsx");

        if(file.exists() && file.canRead()){
            FileInputStream workbookLocation = new FileInputStream(file);

        XSSFWorkbook workbook = new XSSFWorkbook(workbookLocation);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Row row1 = sheet.getRow(1);
        Cell cell1C0=row1.getCell(0);
        Cell cell1C1=row1.getCell(1);

        String emailRow1= cell1C0.toString();
        String passwordRow1=cell1C1.toString();

        ShopLoginPage shopLoginPage = new ShopLoginPage();
        shopLoginPage.getEmail().sendKeys(emailRow1);
        shopLoginPage.getPassword().sendKeys(passwordRow1);
        Thread.sleep(3000);
        shopLoginPage.getSignInButton().click();

        ShopYourAccount shopYourAccount =  new ShopYourAccount();

        try{
            shopYourAccount.getSignOutButton().click();
            ExtentManager.pass("User has signed in");
        }catch (Exception e)
        {
            ExtentManager.fail("User could not sign it");
            Assert.fail();
        }


        Row row2 = sheet.getRow(2);
        Cell cell2C0=row2.getCell(0);
        Cell cell2C1=row2.getCell(1);

        String emailRow2= cell2C0.toString();
        String passwordRow2=cell2C1.toString();

        shopLoginPage.getEmail().sendKeys(emailRow2);
        shopLoginPage.getPassword().sendKeys(passwordRow2);
        waitToBeVisible(shopLoginPage.getSignInButton());
        shopLoginPage.getSignInButton().click();
            try{
                shopYourAccount.getSignOutButton().click();
                ExtentManager.pass("User has signed in");
            }catch (Exception e)
            {
                ExtentManager.fail("User could not sign it");
                Assert.fail("User could not sign it");
            }
        }
    }
}
