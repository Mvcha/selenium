import base.BasePage;
import base.ExtentManager;
import base.Hooks;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.*;

import java.io.IOException;

@Listeners(Listener.class)
public class OrderCompleteTest extends Hooks {

    public OrderCompleteTest() throws IOException {
        super();
    }


    @Test
    public void endToEndTest() throws InterruptedException, IOException {
        ExtentManager.log("Starting Order Complete Test");
        HomePage homePage=new HomePage();
        if(homePage.getCookiePopup().isDisplayed())
        {
            homePage.getCookiePopup().click();
        }
        homePage.getTestStoreLink().click();
        ExtentManager.pass("Reached the shop homepage");
        ShopHomePage shopHomePage=new ShopHomePage();
        shopHomePage.getProductOne().click();

        ShopProductPage shopProductPage = new ShopProductPage();
        ExtentManager.pass("Reached the shop product page");
        Select option = new Select(shopProductPage.getSizeOption());
        option.selectByVisibleText("M");
        ExtentManager.pass("Successfully selected product size");
        shopProductPage.getQuantityIncrease().click();
        ExtentManager.pass("Successfully increased product quantity");
        shopProductPage.getAddToCartButton().click();

        ShopContentPanel shopContentPanel=new ShopContentPanel();
        waitForElementInvisible(shopContentPanel.getCheckOutButton());
        shopContentPanel.getCheckOutButton().click();

        ShoppingCart shoppingCart=new ShoppingCart();
        ExtentManager.pass("Successfully opened shopping cart");
        shoppingCart.getHavePromo().click();
        shoppingCart.getPromoTextBox().sendKeys("20OFF");
        ExtentManager.pass("Successfully added promo code");
        shoppingCart.getPromoAddButton().click();
        shoppingCart.getProceedToCheckOutButton().click();
        ExtentManager.pass("Successfully added proceeded checkout");

        OrderFormPersonalInformation orderFormPersonalInformation=new OrderFormPersonalInformation();
        orderFormPersonalInformation.getGenderMr().click();
        orderFormPersonalInformation.getFirstNameField().sendKeys("Jan");
        orderFormPersonalInformation.getLastNameField().sendKeys("Kowalski");
        orderFormPersonalInformation.getEmailField().sendKeys("j.kowalski@gmail.com");
        orderFormPersonalInformation.getTermsConditionsCheckbox().click();
        orderFormPersonalInformation.getContinueButton().click();

        OrderFormDelivery orderFormDelivery = new OrderFormDelivery();
        waitToBeVisible(orderFormDelivery.getCompanyField());
        orderFormDelivery.getAddressField().sendKeys("Ulicowa 23");
        orderFormDelivery.getCityField().sendKeys("Lublin");
        Select state = new Select(orderFormDelivery.getStateDropdown());
        state.selectByVisibleText("Ohio");
        orderFormDelivery.getPostcodeField().sendKeys("22033");
        orderFormDelivery.getContinueButton().click();

        OrderFormShippingMethod orderFormShippingMethod = new OrderFormShippingMethod();
        orderFormShippingMethod.getDeliveryMessageTextbox().sendKeys("Short note");
        ExtentManager.pass("Successfully added note to delivery");
        orderFormShippingMethod.getContinueButton().click();

        OrderFormPayment orderFormPayment = new OrderFormPayment();
        orderFormPayment.getPayByCheckRadioButton().click();
        orderFormPayment.getTermsConditionsCheckbox().click();
        orderFormPayment.getOrderButton().click();
        ExtentManager.pass("Successfully finished order");

    }
}
