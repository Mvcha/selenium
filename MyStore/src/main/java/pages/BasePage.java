package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

public class BasePage {
    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public WebDriver driver;
    public Actions actions;
    public WebDriverWait wait;

    public void waitToBeVisible(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public double getPrice(WebElement element){
        return Double.parseDouble(element.getText().replace("$", ""));
    }

    public String getValue(WebElement element) {
        return element.getAttribute("value");
    }

    public void click(WebElement element) {
        System.out.println("Clicking on: " + element.getText());
        element.click();
    }

    public void sendKeys(WebElement element, String text) {
        System.out.println("Typing: " + text);
        element.sendKeys(text);
    }

    public void sendKeysAndClear(WebElement element, String text) {
        element.clear();
        sendKeys(element, text);
    }

    public WebElement getRandomElement(List<WebElement> elements) {
        return elements.get(new Random().nextInt(elements.size()));
    }

    public boolean isDisplayed(By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
