package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ProductsGridPage extends BasePage {

    public ProductsGridPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".js-product-miniature")
    private List<WebElement> productMiniatures;

    public List<ProductMiniaturePage> getProductMiniatures() {
        return productMiniatures.stream().map(ProductMiniaturePage::new).collect(Collectors.toList());
    }

    public void openProductWithName(String productToOpen) {
        for (ProductMiniaturePage productMiniaturePage : getProductMiniatures()) {

            if (productMiniaturePage.getName().equals(productToOpen)) {
                productMiniaturePage.open();
                break;
            }
        }
    }
    public void openRandomProduct()
    {
        //1 sposob
       // getRandomElement(this.productMiniatures).click();

        //2 sposob ale jest pewnosc ze zostanie kliniety w dobre miejsce a nie jakies szybki podglad produktu
        new ProductMiniaturePage(getRandomElement(productMiniatures)).open();
    }
}
