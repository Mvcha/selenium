package pages.mountains;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class MountainRowPage {
    public MountainRowPage(WebElement row) {
        PageFactory.initElements(new DefaultElementLocatorFactory(row), this);
    }

    @FindBy(xpath = "td[1]")
    private WebElement name;

    @FindBy(xpath = "td[2]")
    private WebElement state;

    @FindBy(xpath = "td[3]")
    private WebElement mountainRange;
    @FindBy(xpath = "td[4]")
    private WebElement height;

    public String getName() {
        return name.getText();
    }

    public int getHeight() {
        return Integer.parseInt(height.getText());
    }
}
