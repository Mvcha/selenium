package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductDetailsPage extends BasePage{

    public ProductDetailsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".product-container h1")
    private WebElement productName;
    @FindBy(css = "#quantity_wanted")
    private WebElement quantityInput;
    @FindBy(css = ".add-to-cart")
    private WebElement addToBasketBtn;
    @FindBy(css = "[itemprop='price']")
    private WebElement price;
    public String getName() {
        return productName.getText();
    }
    public int getQuantity() {
        return Integer.parseInt(getValue(quantityInput));
    }
    public void setQuantity(int quantity) {
        sendKeysAndClear(quantityInput, String.valueOf(quantity));
    }
    public double getPrice() {
        return getPrice(price);
    }
    public void addToBasket() {
        click(addToBasketBtn);
        waitToBeVisible(By.cssSelector("#blockcart-modal"));
    }
}
