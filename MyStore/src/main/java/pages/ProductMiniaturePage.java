package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

import java.util.List;

public class ProductMiniaturePage {

    public ProductMiniaturePage(WebElement miniature) {
        PageFactory.initElements(new DefaultElementLocatorFactory(miniature), this);
    }

    @FindBy(css = ".product-title")
    WebElement productName;

    public String getName() {
        return productName.getText();
    }

    public void open(){
        productName.click();
    }
}
