package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartModalPage  extends BasePage{

    public CartModalPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(css = "#blockcart-modal .product-name")
    private WebElement productName;

    @FindBy(css = "#blockcart-modal .btn-secondary")
    private WebElement continueShoppingBtn;

    @FindBy(css = "#blockcart-modal .btn-primary")
    private WebElement proceedToCheckoutBtn;

    public String getProductName()
    {
        return productName.getText();
    }
    public void continueShopping()
    {
        click(continueShoppingBtn);
    }

    public void proceedToCheckout()
    {
        click(proceedToCheckoutBtn);
    }
}
