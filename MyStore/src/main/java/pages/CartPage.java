package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class CartPage extends  BasePage{
    public CartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#cart-subtotal-products .value")
    private WebElement totalPrice;
    @FindBy(css = ".cart-item")
    private List<WebElement> products;

    public List<CartItemPage> getCartItems() {
        List<CartItemPage> cartItems = new ArrayList<>();
        for (WebElement cartItem : products) {
            cartItems.add(new CartItemPage(cartItem));
        }
        return cartItems;
    }

    public double getTotalPrice(){
        return getPrice(totalPrice);
    }
}
