package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class CartItemPage {
    public CartItemPage(WebElement cartItem) {
        PageFactory.initElements(new DefaultElementLocatorFactory(cartItem), this);
    }

    @FindBy(css = ".product-line-info a")
    private WebElement name;

    @FindBy(css = "[class='price']")
    private WebElement price;

    @FindBy(css = ".js-cart-line-product-quantity")
    private WebElement quantity;

    @FindBy(css = "[class='product-price']")
    private WebElement totalPrice;

    public String getName(){
        return name.getText();
    }

    public double getPrice(){
        return Double.parseDouble(price.getText().replace("$", ""));

    }
    public double getTotalPrice(){
        return Double.parseDouble(totalPrice.getText().replace("$", ""));

    }
    public int getQuantity(){
        return Integer.parseInt(quantity.getAttribute("value"));
    }
}
