package models;

import pages.CartItemPage;
import pages.CartPage;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Product> products;
    private double orderTotalPrice;

    public Order(CartPage cartPage) {
        orderTotalPrice = cartPage.getTotalPrice();
        products = new ArrayList<>();

        for (CartItemPage cartItemPage : cartPage.getCartItems()){
            products.add(new Product(cartItemPage));
        }
    }


    public Order() {
        this.products = new ArrayList<>();
    }

    public void addProduct(Product productToAdd) {
        for (Product product : products) {
            if (product.getName().equals(productToAdd.getName())) {
                product.addQuantity(productToAdd.getQuantity());
                orderTotalPrice += productToAdd.getTotalPrice();
                return;
            }
        }
        products.add(productToAdd);
        orderTotalPrice += productToAdd.getTotalPrice();
    }

    public void deleteProduct(int index){
        // implmentacja usuwania produkt
    }

    public void deleteAll(){
        // czyszczenie listy
    }
}

