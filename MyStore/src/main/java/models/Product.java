package models;

import pages.CartItemPage;
import pages.ProductDetailsPage;

public class Product {
    private String name;
    private int quantity;
    private double totalPrice;
    private double price;

    public Product(CartItemPage cartItemPage) {
        name = cartItemPage.getName();
        quantity = cartItemPage.getQuantity();
        price = cartItemPage.getPrice();
        totalPrice = cartItemPage.getTotalPrice();
    }

    public Product(ProductDetailsPage productDetailsPage) {
        name = productDetailsPage.getName();
        quantity = productDetailsPage.getQuantity();
        price = productDetailsPage.getPrice();
        totalPrice = price * quantity;
    }

    public Product(String name, int quantity, double price, double totalPrice) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }


    public double getTotalPrice() {
        return totalPrice;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void addQuantity(int quantity){
        this.quantity += quantity;
        totalPrice = price * this.quantity;
    }
}
