import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;

public class ProgressBar extends TestBase {

    @Test
    public void shouldWaitForProgressBarWithImplicit(){
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.get("http://51.75.61.161:9102/progressbar.php");

        driver.findElement(By.cssSelector(".ui-progressbar-complete"));

    }

    @Test
    public void shouldWaitForProgressBarWithExplicit(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        driver.get("http://51.75.61.161:9102/progressbar.php");


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ui-progressbar-complete")));

    }

}
