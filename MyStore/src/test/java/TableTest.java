import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pages.mountains.MountainRowPage;
import pages.mountains.MountainsTablePage;

import java.util.List;

public class TableTest extends TestBase {

    @Test
    public void shouldPrintAllMountainsOver4000m() {
        driver.get("http://51.75.61.161:9102/table.php");

        List<WebElement> allRows = driver.findElements(By.cssSelector("tbody tr"));

        // pętla działająca na allRows, która wypisze nazwy gór wyzszych niż 4000m
        for (WebElement row : allRows) {
            WebElement heightV1 = row.findElement(By.xpath("td[4]"));

            //  inny sposób znalezienia n-tego 'td' wewnątrz wiersza
            //  WebElement heightV2 = row.findElements(By.cssSelector("td")).get(3);

            if (Integer.parseInt(heightV1.getText()) > 4000) {
                System.out.println(row.findElement(By.xpath("td[1]")).getText());
            }
        }
    }

    @Test
    public void shouldPrintAllMountainsOver4000mV2() {
        driver.get("http://51.75.61.161:9102/table.php");

        for (MountainRowPage mountainRowPage : new MountainsTablePage(driver).getAllMountains()){
            if(mountainRowPage.getHeight()>4000){
                System.out.println(mountainRowPage.getName());
            }
        }
    }
}
