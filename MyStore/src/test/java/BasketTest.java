import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CartModalPage;
import pages.ProductDetailsPage;
import pages.ProductsGridPage;

public class BasketTest extends TestBase {

    @Test
    public void shouldAddProductToBasket()
    {
        ProductsGridPage productsGridPage = new ProductsGridPage(driver);
        CartModalPage cartModalPage=new CartModalPage(driver);
        ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);

        productsGridPage.openProductWithName("HUMMINGBIRD SWEATER");
        productDetailsPage.addToBasket();
        Assert.assertEquals(cartModalPage.getProductName(),"HUMMINGBIRD SWEATER");
        // dobrze byłoby sprawdzić więcej rzeczy
    }
}
