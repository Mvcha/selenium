import models.Product;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.ProductDetailsPage;
import pages.ProductsGridPage;
import models.Order;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Random;

public class BasketGenericTest extends TestBase{

    @Test(invocationCount = 5)
    public void verifyBasketCalculations(){
        Order expectedOrder = new Order();
        ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);
        CartPage cartPage = new CartPage(driver);

        for (int i = 0; i < 5; i++) {
            driver.get("http://146.59.32.4/index.php");
            new ProductsGridPage(driver).openRandomProduct();

            productDetailsPage.setQuantity(new Random().nextInt(2) + 1);

            expectedOrder.addProduct(new Product(productDetailsPage));

            productDetailsPage.addToBasket();
        }

        driver.get("http://146.59.32.4/index.php?controller=cart&action=show");

        Order actualOrder = new Order(cartPage);

        Assertions.assertThat(actualOrder).usingRecursiveComparison().isEqualTo(expectedOrder);
    }
}
