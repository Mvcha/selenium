# Selenium



## Tests TodoMVC - https://todomvc.com/examples/vanillajs/

TodoMVC is Singe Page Application every tests are in test/java/test, but support methods are in tests/java/pages/


### Test plan
Adding new todo
1. Create new todo.
2. Veryify amount todos with expected amount.
3. Veryify if new todo is on the list.

Editing todo
1. Create new todos.
2. Edit selected todo.
3. Check if the edited todo is on the list.

Completing one todo
1. Create new todos.
2. Checked as completed todo.
3. Check if the completed todo is on the list completed todo.

Completing all todos
1. Create new todos.
2. Checked all todos as completed by button.
3. Veryify amount completed todos.
4. Veryify if todos are on the completed list.

Removing todo
1. Create new todos.
2. Remove selected todo.
3. Veryify that todo doesn't exist on the list.

